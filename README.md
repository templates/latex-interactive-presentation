# ASTRON LaTeX Interactive Presentation

## Features:

- Use LaTeX and have a template to represent ASTRON
- Use gifs and videos in the presentation
- Highlight and draw on slides during the presentation
- Virtual laser pointer
- Dual-head (multi-monitor) setup for notes and time tracking


## Demo

![Demo](resources/demo/demo.webm)

## How to use:

```
git clone https://git.astron.nl/templates/latex-interactive-presentation.git
cd latex-interactive-presentation
texi2pdf presentation.tex
pympress presentation.pdf
```

Change the contents of presentation.tex to your liking.

## Dependencies:

**Please see [pympress](https://pypi.org/project/pympress/#Dependencies)**
**documentation for its specific requirements**

- Python 3.x
- pympress
    - poppler
    - cairo
    - gtk+3
    - python-vlc
    - there are more...
- LaTeX compiler (pick any):
    - texinfo (texi2pdf)
    - LaTeX
    - pdfLaTeX
    - XeLaTeX
    - LuaLaTeX

## Documentation

You can find all documentation and features for presenting on:
[pympress](https://pypi.org/project/pympress/#functionalities)

In addition you can find examples of most functionality through the demo

```
texi2pdf demo.tex
pympress demo.pdf
```

## FAQ:

* 1. I see errors in my console about python object `NoneType` when using pympress:
* 1. You have unmet dependencies see [pympress](https://pypi.org/project/pympress/#Dependencies)
* 2. When I move the .sty files to a separate directory I can't use the theme anymore
* 2. The directory structure can not be changed
